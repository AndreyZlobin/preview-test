import VueRouter from 'vue-router';
import PageLoader from '~vue/modules/PageLoader/PageLoader.vue';
import PageError from '~vue/modules/PageError/PageError.vue';

function lazyLoadView(AsyncView) {
	const AsyncHandler = () => ({
		component: AsyncView,
		loading: PageLoader,
		delay: 400,
		error: PageError,
		timeout: 30000,
	});

	return Promise.resolve({
		functional: true,
		render(h, { data, children }) {
			return h(AsyncHandler, data, children);
		},
	});
}

const pageFabric = (component) => ({
	component,
	loading: PageLoader,
	error: PageError,
	delay: 0,
	timeout: 100,
});

const Personal = () =>
	lazyLoadView(import(/* webpackChunkName: "pagePersonal" */ '~vue/pages/Personal/Personal.vue'));
const Auth = () =>
	lazyLoadView(import(/* webpackChunkName: "pageAuth" */ '~vue/pages/Auth/Auth.vue'));

const PageGranting = () =>
	lazyLoadView(
		import(
			/* webpackChunkName: "pageGranting" */ '~vue/pages/Personal/PageGranting/PageGranting.vue'
		)
	);
const PageAsp = () =>
	lazyLoadView(import(/* webpackChunkName: "pageAsp" */ '~vue/pages/Personal/PageAsp/PageAsp.vue'));
const SupportPage = () =>
	lazyLoadView(
		import(/* webpackChunkName: "supportPage" */ '~vue/pages/Personal/SupportPage/SupportPage.vue')
	);
const PageActiveContracts = () =>
	lazyLoadView(
		import(
			/* webpackChunkName: "pageActiveContracts" */ '~vue/pages/Personal/PageActiveContracts/PageActiveContracts.vue'
		)
	);
const PageHistoryContracts = () =>
	lazyLoadView(
		import(
			/* webpackChunkName: "pageHistoryContracts" */ '~vue/pages/Personal/PageHistoryContracts/PageHistoryContracts.vue'
		)
	);
const PagePaymentSchedule = () =>
	lazyLoadView(
		import(
			/* webpackChunkName: "pagePaymentSchedules" */ '~vue/pages/Personal/PagePaymentSchedule/PagePaymentSchedule.vue'
		)
	);
const Page404 = () =>
	lazyLoadView(import(/* webpackChunkName: "page404" */ '~vue/pages/Personal/Page404/Page404.vue'));
const PageModals = () =>
	lazyLoadView(
		import(/* webpackChunkName: "pageModals" */ '~vue/pages/Personal/PageModals/PageModals.vue')
	);
const PagePaydone = () =>
	lazyLoadView(
		import(/* webpackChunkName: "pagePaydone" */ '~vue/pages/Personal/PagePaydone/PagePaydone.vue')
	);
const PagePayerror = () =>
	lazyLoadView(
		import(
			/* webpackChunkName: "pagePaydone" */ '~vue/pages/Personal/PagePayerror/PagePayerror.vue'
		)
	);

const router = new VueRouter({
	routes: [
		{
			path: '/personal',
			component: Personal,
			meta: {
				requiresAuth: true,
			},
			children: [
				{ path: '/personal/asp', component: PageAsp },
				{ path: '/personal/granting', component: PageGranting, name: 'granting' },
				{ path: '/personal/history', component: PageHistoryContracts, name: 'history' },
				{ path: '/personal/modals', component: PageModals },
				{ path: '/personal/support', component: SupportPage },
				{
					path: '/personal/schedule/:id',
					component: PagePaymentSchedule,
					name: 'schedule',
					props: true,
				},
				{ path: '/personal/paydone', component: PagePaydone },
				{ path: '/personal/payerror', component: PagePayerror },
				{ path: '/personal/', component: PageActiveContracts, name: 'home' },
				{ path: '/personal/*', component: Page404 },
			],
		},
		{
			path: '/auth',
			component: Auth,
			name: 'auth',
			props: true,
		},
		{
			path: '/',
			component: Auth,
		},
		{
			path: '/*',
			component: { render: (h) => h('div', ['404! Page Not Found!']) },
		},
	],
});

export default router;
