import apiUrlCollection from '~/API/Auth/AuthApi';
import APIRequest from '~/API/APIRequest';

export default {
    showAuthModal({ commit }, payload) {
        commit('setToStateShowAuthStatus', payload);
    },

    fetchUserPhone({ commit }, payload) {
        commit('setUserPhoneToState', payload);
    },

    loaderToggle({ commit }, payload) {
        commit('setToStateNewLoaderStatus', payload);
    },

    fetchUserSmsCode({ commit }, payload) {
        commit('setToStateUserSmsCode', payload);
    },

    goToStep({ commit, state }, step) {
        const availableSteps = new Set(state.stepsList);
        if (step && availableSteps.has(step)) {
            commit('setToStateNewLoaderStatus', false);
            commit('changeCurrentStep', step);
            commit('setErrorText', null);
        }
    },

    async checkSmsCode({ commit, dispatch }, payload) {
        try {
            const baseUrl = apiUrlCollection.checkSmsCode;
            const request = await APIRequest.PostRequest(payload, baseUrl);
            const response = await request;
            if (response.status === 'success') {
                dispatch('goToStep', 'AuthNewPasswordForm');
                this.metrika('codeSuccess');
            } else {
                commit('setErrorText', response.error_text);
            }
        } catch (error) {
            return dispatch('goToStep', 'ErrorConnection');
        }
    },
    // **************************************
    // и т.п
};
