export default {
	showAuthModal: true,
	isAuth: false,
	userPhone: '',
	userSmsCode: '',
	showLoader: false,
	errorText: null,
};
