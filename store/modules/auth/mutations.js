export default {
    setToStateShowAuthStatus(state, status) {
        state.showAuthModal = status;
    },
    setUserPhoneToState(state, userPhone) {
        state.userPhone = userPhone;
    },
};
