export default {
    getShowAuthModalStatus(state) {
        return state.showAuthModal;
    },

    getUserPhone(state) {
        return state.userPhone;
    },

    getUserSmsCode(state) {
        return state.userSmsCode;
    },

    getCurrentStep(state) {
        return state.currentStep;
    },

    getLoaderStatus(state) {
        return state.showLoader;
    },

    getTimerCount(state) {
        return state.timer.count
    },

    getTimerTotal(state) {
        return state.timer.total
    },

    getErrorText(state) {
        return state.errorText;
    }
}
