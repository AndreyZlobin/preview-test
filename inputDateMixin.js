import { Component, Prop } from 'vue-property-decorator';
import { requiredIf } from 'vuelidate/lib/validators';
import BaseFormComponent from '../components/FormComponents/*********/*********';
import maskCollection from '../../scripts/parts/*****/*****';

@Component({
	validations: {
		dataValue: {
			required: requiredIf(function () {
				return this.required;
			}),
			ageValidate: (value) => {
				// эта логику у меня вынесена, т.к демо, решил сюда положить, показать
				const filteredValue = value.replace(/\s+/g, '');
				if (filteredValue.length) {
					let date = filteredValue.split('.');
					date = `${date[2]}-${date[1]}-${date[0]}`;
					date = new Date(date);
					date = new Date() - date;
					date = date / 3600 / 24 / 1000 / 365.25;
					return date ? !(date < 21 || date > 75) : false;
				}
				return false;
			},
		},
	},
})
export default class BaseInputDateComponent extends BaseFormComponent {
	@Prop({ type: String, default: 'text' }) typeInput;

	@Prop({ type: Number, default: 75 }) maxAge;

	@Prop({ type: Number, default: 21 }) minAge;

	@Prop({ type: Boolean, default: false }) showTitle;

	get invalid() {
		return this.$v.$invalid;
	}

	get getErrors() {
		return this.$v.dataValue.$error && this.$v.dataValue.$dirty;
	}

	get getSuccess() {
		return !this.$v.dataValue.$error && this.$v.dataValue.$dirty;
	}

	get dateLengthErrorText() {
		if (this.$v.dataValue.$dirty && !this.$v.dataValue.ageValidate && this.dataValue) {
			return `Допустимый возраст от ${this.minAge} до ${this.maxAge} лет`;
		}
	}

	get computedClass() {
		return {
			'cf-input--focus': this.focus,
			'cf-input--error': this.getErrors,
			'cf-input--success': this.getSuccess,
		};
	}

	get errorTextRequired() {
		return this.getErrors && !this.dataValue;
	}

	createMask() {
		const maskInput = maskCollection.get(this.typeInput);
		if (maskInput) {
			maskInput.mask(this.$refs.input);
		}
	}

	validate() {
		this.$v.dataValue.$touch();
		return this.getErrors;
	}

	onInput(event) {
		const { value } = event.target;
		this.dataValue = value;
		this.$emit('on-input', { value: this.dataValue, hasError: this.hasErrors });
	}

	onFocus() {
		this.focus = true;
		this.$v.dataValue.$reset();
	}

	onBlur() {
		this.focus = false;
		this.validate();
	}

	mounted() {
		this.dataValue = this.params.value;
		this.createMask();
	}
}
